# 算法







### 

|                            |                          |
| -------------------------- | ------------------------ |
| BrFS(BFS)                  | Breadth-first-search     |
| DFS                        | Deep-first-search        |
| GBFS                       | Greedy Best-First Search |
| BFWS                       | Best-First Width Search  |
| $h^*,h^+,h^{add},h^{max},$ |                          |
| IW                         |                          |
| UCT                        |                          |
|                            |                          |
|                            |                          |

